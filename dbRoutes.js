const Organization = require('./models').Organization;

module.exports = router => {

    router.get('/count', (req, res) => {
        console.log('hello count')
       Organization.count(req.query)
       .then(count => res.json(count))
       .catch(err => {
           console.log(err);
           return res.status(400).send(err.message)
       })
    
    })
    
    
    
    // CRUD routes
    router.get('/', (req, res) => {
        const { sort, skip, limit, one, ...rest} = req.query;
        let q;
        if (!rest || Object.keys(rest || {}).length === 0) {
            return res.json([]);
        }
    
        Object.keys(rest).forEach((key) => {
            if (key.indexOf('$') === 0) {
                const val = rest[key];
                rest[key] = [];
                Object.keys(val).forEach((subKey) => {
                    val[subKey].forEach(subVal => {
                        rest[key].push({ [subKey]: subVal })
                    })
                })
            }
        })
        console.log(rest)
        if (one) {
            q = Organization.findOne(rest);
        } else {
            q = Organization.find(rest);
        }
        if (sort) {
            Object.keys(sort).forEach(key => {
                q.sort({ [key]: parseInt(sort[key]) })
            })
        }
        if (skip) {
            q.skip(parseInt(skip))
        }
        if (limit) {
            q.limit(parseInt(limit))
        }
        q.then((organizations) => {
            return res.json(organizations);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })
    
    router.post('/', (req, res) => {
        const data = req.body;
        Organization.create(data)
        .then((organization) => {
            return res.json(organization);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })
    
    router.patch('/', (req, res) => {
        let { conditions, values, options } = req.body;
        if (!options) {
            options = {};
        }
        Organization.update(conditions, { $set: values }, { ...options, multi: true })
        .then(() => Organization.find(conditions))
        .then(organizations => {
            return res.json(organizations);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })
    
    router.delete('/', (req, res) => {
        let conditions = req.body;
        let organizations;
        Organization.find(conditions)
        .then((a) => {
            organizations = a;
            return Organization.remove(conditions)
        })
        .then(() => {
            return res.json(organizations);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })
    
    router.get('/:id', (req, res) => {
        Organization.findById(req.params.id)
        .then((organization) => {
            return res.json(organization);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })
    
    router.patch('/:id', (req, res) => {
        const { id } = req.params;
        const changes = req.body;
        Organization.findByIdAndUpdate(id, { $set: changes })
        .then(() => Organization.findById(id))
        .then(organization => {
            return res.json(organization);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })
    
    router.delete('/:id', (req, res) => {
        const { id }  = req.params;
        let deletedOrganization;
        Organization.findById(id)
        .then(organization => {
            deletedOrganization = organization;
            return Organization.findByIdAndRemove(id)
        })
        .then(() => {
            return res.json(deletedOrganization);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })
    return router;
}