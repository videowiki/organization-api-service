const fs = require('fs');
const mongoose = require('mongoose')
const videowikiGenerators = require('@videowiki/generators');
const { server, app, createRouter } = require('./generateServer')();
const controller = require('./controller');
const middlewares = require('./middlewares');

const multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/tmp')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.' + file.originalname.split('.').pop())
    }
})
var upload = multer({ storage: storage })
const DB_CONNECTION = process.env.ORGANIZATION_SERVICE_DATABASE_URL;

let mongoConnection
mongoose.connect(DB_CONNECTION)
.then((con) => {
    mongoConnection = con.connection;
    con.connection.on('disconnected', () => {
        console.log('Database disconnected! shutting down service')
        process.exit(1);
    })

    videowikiGenerators.healthcheckRouteGenerator({ router: app, mongoConnection })
    app.use('/db', require('./dbRoutes')(createRouter()))

    app.all('*', (req, res, next) => {
        if (req.headers['vw-user-data']) {
            try {
                const user = JSON.parse(req.headers['vw-user-data']);
                req.user = user;
            } catch (e) {
                console.log(e);
            }
        }
        next();
    })

    app.post('/', upload.any(), middlewares.validateImagesExtension('multiple'), middlewares.authorizeUser, controller.createOrganization);
    app.patch('/:organizationId/logo', upload.single('logo'), middlewares.validateImagesExtension('single'), middlewares.authorizeOwnerAndAdmin, controller.updateLogo)

    // This route is deprecated, remove by November
    // app.post('/:organizationId/invitations/respond', controller.respondToInvitation)
    app.post('/:organizationId/invitations/respondAuth', controller.respondToInvitationAuth)

    app.post('/:organizationId/users', middlewares.authorizeOwnerAndAdmin, controller.addUser)

    app.patch('/:organizationId/users/:userId/permissions', middlewares.authorizeOwnerAndAdmin, controller.editPermissions)
    app.delete('/:organizationId/users/:userId', middlewares.authorizeOwnerAndAdmin, controller.removeUser);

    app.get('/:organizationId', controller.getById)


})
.catch(err => {
    console.log('mongo connection error', err);
    process.exit(1);
})



const PORT = process.env.PORT || 4000;
server.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
exports = module.exports = app             // expose app
