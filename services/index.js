const {
    AUTH_SERVICE_API_ROOT,
    ARTICLE_SERVICE_API_ROOT,
    COMMENT_SERVICE_API_ROOT,
    ORGANIZATION_SERVICE_API_ROOT,
    USER_SERVICE_API_ROOT,
    VIDEO_SERVICE_API_ROOT,
    NOTIFICATION_SERVICE_API_ROOT,
    WEBSOCKETS_SERVICE_API_ROOT,
    EMAIL_SERVICE_API_ROOT,
    FRONTEND_HOST_NAME,
    FRONTEND_HOST_PROTOCOL,
    STORAGE_SERVICE_API_ROOT,
    TEXT_TO_SPEECH_SERVICE_API_ROOT,
    TRANSLATION_SERVICE_API_ROOT,
    TRANSLATION_EXPORT_SERVICE_API_ROOT,
    SUBTITLES_SERVICE_API_ROOT
} = process.env;

console.log('vide service', VIDEO_SERVICE_API_ROOT)

const authService = require('@videowiki/services/auth')(AUTH_SERVICE_API_ROOT);
const articleService = require('@videowiki/services/article')(ARTICLE_SERVICE_API_ROOT);
const commentService = require('@videowiki/services/comment')(COMMENT_SERVICE_API_ROOT);
const userService = require('@videowiki/services/user')(USER_SERVICE_API_ROOT);
const videoService = require('@videowiki/services/video')(VIDEO_SERVICE_API_ROOT);
const notificationService = require('@videowiki/services/notification')({ API_ROOT: NOTIFICATION_SERVICE_API_ROOT, WEBSOCKETS_API_ROOT: WEBSOCKETS_SERVICE_API_ROOT });
const translationExportService = require('@videowiki/services/translationExport')(TRANSLATION_EXPORT_SERVICE_API_ROOT)
const subtitlesService = require('@videowiki/services/subtitles')(SUBTITLES_SERVICE_API_ROOT);

const emailService = require('@videowiki/services/email')({ EMAIL_SERVICE_API_ROOT, FRONTEND_HOST_NAME, FRONTEND_HOST_PROTOCOL })
const storageService = require('@videowiki/services/storage')(STORAGE_SERVICE_API_ROOT);
const websocketsService = require('@videowiki/services/websockets')(WEBSOCKETS_SERVICE_API_ROOT);
const websocketsRooms = require('@videowiki/services/websockets/rooms');
const websocketsEvents = require('@videowiki/services/websockets/events');

const translationService = require('@videowiki/services/translation')(TRANSLATION_SERVICE_API_ROOT);
const textToSpeechService = require('@videowiki/services/textToSpeach')(TEXT_TO_SPEECH_SERVICE_API_ROOT);

module.exports = {
    authService,
    articleService,
    commentService,
    userService,
    videoService,
    notificationService,
    emailService,
    storageService,
    websocketsService,
    websocketsRooms,
    websocketsEvents,
    translationService,
    textToSpeechService,
    translationExportService,
    subtitlesService,
}